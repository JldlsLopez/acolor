/**
 *
 * @author Jorge lopez
 * @version 20150622
 */

package persistencia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;



public class ManejarDatos 
{
    private File archivo;
    
    //Constructor
    public ManejarDatos()
    {
      archivo = null;
    }
    
    public File getArchivo()
    {
      return archivo;
    }
    
    public void crearArchivo(String nombre)
    {
      archivo = new File(nombre);
    }
    
    public void editarYGualdarArchivo(String texto)
    {
        try {
            FileWriter archivoEditable = new FileWriter(archivo, false);
            PrintWriter editarArchivo = new PrintWriter(archivoEditable);
            editarArchivo.write(texto);
            editarArchivo.flush();
            editarArchivo.close();
        } catch (IOException ex) {
            
        }
    }
    
    public String leerArchivo() throws IOException
    {
        String valor = "";
        try {
            FileReader archivoALeer = new FileReader(archivo);
            BufferedReader lector = new BufferedReader(archivoALeer);
            valor = lector.readLine();
            lector.close();
        } catch (FileNotFoundException ex) {
            
        }
        return valor;
    }
}
