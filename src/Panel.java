/**
 *
 * @author Jorge lopez
 * @version 20150622
 */
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import java.awt.Graphics;

public class Panel extends JPanel{
    
    String urlPicture;
    ImageIcon picture;
    
    public Panel(){
    super();
    urlPicture = "/picture/color.png";
    setSize(400, 600);
    }
    
    @Override
    public void paintComponent(Graphics g){
    Dimension size = getSize();
    picture = new ImageIcon(new ImageIcon(getClass().getResource(urlPicture)).getImage());
    g.drawImage(picture.getImage(), 0, 0, size.width, size.height,null);
    }
}
