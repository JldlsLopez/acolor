/**
 *
 * @author Jorge lopez
 * @version 20150622
 */
import java.util.Random;
import java.awt.Color;
import java.util.ArrayList;

public class Colores 
{
    private ArrayList<Color> listColor;
    private final Random random;
    private int idColorActual;
    private String colorEnTexto, colorEnCodigo;
    public Colores()
    {
       listColor = new ArrayList<>();
       listColor.add(Color.WHITE); listColor.add(Color.BLACK); listColor.add(Color.GRAY); 
       listColor.add(Color.GREEN); listColor.add(Color.YELLOW); listColor.add(Color.BLUE); 
       listColor.add(new Color(255, 0, 153)); listColor.add(new Color(255, 0, 0)); listColor.add(new Color(255, 153, 0));
       random = new Random();
       idColorActual = 0;
       colorEnTexto = "";
       colorEnCodigo = "";
    }
    
    public Color colorAleatorio(int arrayLength)
    {
     idColorActual = random.nextInt(arrayLength);
     colorEnCodigo = listColor.get(random.nextInt(arrayLength)).toString();
     return listColor.get(idColorActual);
    }
    
    public int getIdColorActual()
    {
      return idColorActual;
    }
    
    public ArrayList<Color> getListColor()
    {
      return listColor;
    }
    
    @Override
    public String toString()
    {
      if(colorEnCodigo.equals("java.awt.Color[r=255,g=255,b=255]"))
      {
        colorEnTexto = "White";
      }else if(colorEnCodigo.equals("java.awt.Color[r=0,g=0,b=0]"))
      {
        colorEnTexto = "Black";
      }else if(colorEnCodigo.equals("java.awt.Color[r=128,g=128,b=128]"))
      {
        colorEnTexto = "Gray";
      }else if(colorEnCodigo.equals("java.awt.Color[r=0,g=255,b=0]"))
      {
        colorEnTexto = "Green";
      }else if(colorEnCodigo.equals("java.awt.Color[r=255,g=255,b=0]"))
      {
        colorEnTexto = "Yellow";
      }else if(colorEnCodigo.equals("java.awt.Color[r=0,g=0,b=255]"))
      {
        colorEnTexto = "Blue";
      }else if(colorEnCodigo.equals("java.awt.Color[r=255,g=0,b=153]"))
      {
        colorEnTexto = "Pink";
      }else if(colorEnCodigo.equals("java.awt.Color[r=255,g=0,b=0]"))
      {
        colorEnTexto = "Red";
      }else if(colorEnCodigo.equals("java.awt.Color[r=255,g=153,b=0]"))
      {
        colorEnTexto = "Orange";
      }
      return colorEnTexto;
    }
    
    public String uncodingColor(String rbg)
    {
      String colorDescodificado = "";
      if(rbg.equals("java.awt.Color[r=255,g=255,b=255]"))
      {
        colorDescodificado = "White";
      }else if(rbg.equals("java.awt.Color[r=0,g=0,b=0]"))
      {
        colorDescodificado = "Black";
      }else if(rbg.equals("java.awt.Color[r=128,g=128,b=128]"))
      {
        colorDescodificado = "Gray";
      }else if(rbg.equals("java.awt.Color[r=0,g=255,b=0]"))
      {
        colorDescodificado = "Green";
      }else if(rbg.equals("java.awt.Color[r=255,g=255,b=0]"))
      {
        colorDescodificado = "Yellow";
      }else if(rbg.equals("java.awt.Color[r=0,g=0,b=255]"))
      {
        colorDescodificado = "Blue";
      }else if(rbg.equals("java.awt.Color[r=255,g=0,b=153]"))
      {
        colorDescodificado = "Pink";
      }else if(rbg.equals("java.awt.Color[r=255,g=0,b=0]"))
      {
        colorDescodificado = "Red";
      }else if(rbg.equals("java.awt.Color[r=255,g=153,b=0]"))
      {
        colorDescodificado = "Orange";
      }
      return colorDescodificado;
    }
}
